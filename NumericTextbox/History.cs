﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NumericTextBoxLib
{
    public class History
    {
        public string Value { get; set; }
        public int CursorIndex { get; set; }

        public History(string value, int cursorIndex)
        {
            this.Value = value;
            this.CursorIndex = cursorIndex;
        }
    }
}
