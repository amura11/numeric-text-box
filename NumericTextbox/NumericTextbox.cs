﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Media;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Drawing;

namespace NumericTextBoxLib
{

    /// <summary>
    /// Basic idea on how to build a numeric text box taken from: http://msdn.microsoft.com/en-us/library/ms229644(v=vs.80).aspx
    /// Using OnKeyDown to capture Ctrl + [key] is from this article: http://stackoverflow.com/questions/173593/how-to-catch-control-v-in-c-sharp-app
    /// </summary>
    [ToolboxItem(true), ToolboxBitmap(typeof(TextBox))]
    public partial class NumericTextBox : TextBox
    {
		#region Fields (6) 

        /// <summary>
        /// A flag indicating wehter the value can be a negative number or not
        /// </summary>
        private bool allowNegatives;
        /// <summary>
        /// A flag to indicate whether a pasted value should be converted to meet the current settings
        /// Eg. If we paste -1.5 and the settings currently disallow negatives and are integers convert to 2
        /// </summary>
        private bool convert;
        /// <summary>
        /// The type of number the control accepts
        /// </summary>
        private NumberFormat format;
        /// <summary>
        /// The history that is used to undo and redo
        /// </summary>
        private History[] history;
        /// <summary>
        /// The positions of the history.
        /// historyStart: Indicates where the head of the history is currently at
        /// historyEnd: Indicates where the farthest history item is
        /// historyCurrent: Indicates where the current history item is
        /// </summary>
        private int historyStart, historyEnd, historyCurrent;
        /// <summary>
        /// A flag that indicates that the current text needs to be saved to the history
        /// This is so that undo/redo don't trigger the saveToHistory function
        /// </summary>
        private bool saveNeeded;

		#endregion Fields 

		#region Enums (1) 

        /// <summary>
        /// The format of the value stored in the control
        /// </summary>
        public enum NumberFormat
        {
            NF_INT,
            NF_FLOAT
        }

		#endregion Enums 

		#region Constructors (1) 

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericTextBox"/> class.
        /// </summary>
        public NumericTextBox()
        {
            this.saveNeeded = false;
            base.Text = "";
            this.allowNegatives = true;
            this.format = NumberFormat.NF_INT;
            this.history = new History[50];
            this.historyEnd = this.historyStart = this.historyCurrent = 0;
            this.history[0] = new History(base.Text, this.SelectionStart);
            this.convert = true;

            
            //Setup the new context menu
            this.cutMenuItem = new MenuItem("Cut", this.EVENT_CutClick, Shortcut.CtrlX);
            this.copyMenuItem = new MenuItem("Copy", this.EVENT_CopyClick, Shortcut.CtrlC);
            this.pasteMenuItem = new MenuItem("Paste", this.EVENT_PasteClick, Shortcut.CtrlV);
            this.undoMenuItem = new MenuItem("Undo", this.EVENT_UndoClick, Shortcut.CtrlZ);
            this.redoMenuItem = new MenuItem("Redo", this.EVENT_RedoClick, Shortcut.CtrlY);
            this.selectAllMenuItem = new MenuItem("Select All", this.EVENT_SelectAllClick, Shortcut.CtrlA);

            //Create the new menu
            this.ContextMenu = new ContextMenu();

            //Populate the new menu
            this.ContextMenu.MenuItems.AddRange(new MenuItem[] { cutMenuItem, copyMenuItem, pasteMenuItem, new MenuItem("-"), undoMenuItem, redoMenuItem, new MenuItem("-"), selectAllMenuItem });

            //Handle the popup event for the menu
            this.ContextMenu.Popup += new EventHandler(EVENT_MenuPopup);
        }

		#endregion Constructors 

		#region Properties (4) 

        /// <summary>
        /// Gets or Sets whether the NumbericTextBox can support negative numbers
        /// </summary>
        public bool AllowNegatives
        {
            get
            {
                return this.allowNegatives;
            }
            set
            {
                this.allowNegatives = value;
            }
        }

        /// <summary>
        /// Gets or Sets a flag that indicates whether we should format pasted text to meet the current restrictions
        /// </summary>
        public bool ConvertPastedText
        {
            get
            {
                return this.convert;
            }
            set
            {
                this.convert = value;
            }
        }

        /// <summary>
        /// Gets or Sets the number format that the control accepts.
        /// </summary>
        public NumberFormat Format
        {
            get
            {
                return this.format;
            }
            set
            {
                this.format = value;
                if (this.convert)
                {
                    base.Text = convertText(base.Text);
                }
                else
                {
                    base.Text = "";
                }
            }
        }

        /// <summary>
        /// Gets or sets the current text in the <see cref="T:System.Windows.Forms.TextBox"/>.
        /// If the text being set is not valid an exception is thrown
        /// </summary>
        /// <returns>The text displayed in the control.</returns>
        new public string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (validateText(value))
                {
                    if (this.convert)
                    {
                        base.Text = convertText(value);
                    }
                    else
                    {
                        base.Text = value;
                    }
                }
                else
                {
                    throw new InvalidOperationException("The entered text is of the wrong format");
                }
                
            }
        }

		#endregion Properties 

		#region Methods (5) 

		// Protected Methods (2) 

        protected override void OnDragDrop(DragEventArgs drgevent)
        {
            //base.OnDragDrop(drgevent);
            if (this.AllowDrop && drgevent.Data.GetDataPresent(DataFormats.Text))
            {
                string draggedData = (string)drgevent.Data.GetData(DataFormats.Text);
                if (validateText(draggedData))
                {
                    if (this.convert)
                    {
                        base.Text = convertText(draggedData);
                    }
                    else
                    {
                        base.Text = draggedData;
                    }
                }
            }
        }

        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            //base.OnDragEnter(drgevent);
            if (this.AllowDrop && drgevent.Data.GetDataPresent(DataFormats.Text))
            {
                if (validateText((string)drgevent.Data.GetData(DataFormats.Text)))
                {
                    drgevent.Effect = DragDropEffects.Copy;
                }
                else
                {
                    drgevent.Effect = DragDropEffects.None;
                }
            }
            else
            {
                drgevent.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            if (this.saveNeeded)
            {
                saveToHistory();
                this.saveNeeded = false;
                Console.WriteLine("Saved to history!");
            }
            
        }
		// Private Methods (3) 

        /// <summary>
        /// Converts the text to the specified settings of the control
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>The converted string</returns>
        private string convertText(string text)
        {
            string output;

            //Get the current number format so we can handle different locales
            NumberFormatInfo nfi = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;

            //Remove the negative sign if it's there
            if (!this.allowNegatives && text.Substring(0, 1) == nfi.NegativeSign)
            {
                output = text.Substring(1, text.Length - 1);
            }
            else
            {
                output = text;
            }

            //If it's an int then round it and remove the decimals
            if (this.format == NumberFormat.NF_INT)
            {
                //Parse the value into a float so we can convert it
                float value = float.Parse(output);
                output = Math.Round(value).ToString("#");
            }

            return output;
        }

        /// <summary>
        /// Advances the current position in the history array and saves the current text
        /// If the current position is at the end of the array we wrap around
        /// If the current position has reached the start we push the start over one more
        /// </summary>
        private void saveToHistory()
        {
            if (base.Text != this.history[this.historyCurrent].Value)
            {
                //Increment the current place in the history and wrap if needed
                this.historyCurrent++;
                if (this.historyCurrent >= this.history.Length)
                {
                    this.historyCurrent = 0;
                }

                //Set the end to where the current now is as we don't want to be able to redo passed the most recent modification
                this.historyEnd = this.historyCurrent;

                //Check if our current has hit the start. If so move the start forward one
                if (this.historyCurrent == this.historyStart)
                {
                    this.historyStart++;
                }

                //Write the current text to the current location in the history
                this.history[this.historyCurrent] = new History(base.Text, this.SelectionStart);

                Console.WriteLine("Start: " + this.historyStart + ", Current: " + this.historyCurrent + ", End: " + this.historyEnd);
            }
        }

        /// <summary>
        /// Checks a given bit of text if it is a valid number that we can support
        /// </summary>
        /// <param name="text">The given text to check</param>
        /// <returns>Whether the text is valid or not</returns>
        private bool validateText(string text)
        {
            bool result = true;

            if (text != "")
            {
                //Get the current number format so we can handle different locales
                NumberFormatInfo nfi = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;

                //If we are going to convert we need to make sure it is a valid float and then we are good
                //If not we need to check if it meets the current criteria
                if (this.convert)
                {
                    result = (Regex.IsMatch(text, @"^-{0,1}[\d]*" + Regex.Escape(nfi.NumberDecimalSeparator) + @"{0,1}[\d]*$"));
                }
                else
                {
                    //Check if there is an illegal negative sign
                    if (!this.allowNegatives && text.Substring(0, 1) == nfi.NegativeSign)
                    {
                        result = false;
                    }

                    switch (this.format)
                    {
                        case NumberFormat.NF_INT:
                            result = result && (Regex.IsMatch(text, @"^-{0,1}[\d]*$"));
                            break;
                        case NumberFormat.NF_FLOAT:
                            result = result && (Regex.IsMatch(text, @"^-{0,1}[\d]*" + Regex.Escape(nfi.NumberDecimalSeparator) + @"{0,1}[\d]*$"));
                            break;
                    }
                }
            }
            else
            {
                result = true;
            }

            return result;
        }


        #region Menu Event Handlers

        void EVENT_MenuPopup(Object sender, EventArgs args)
        {
            //We can only cut/paste if there is something selected
            this.copyMenuItem.Enabled = this.cutMenuItem.Enabled = (this.SelectionLength > 0);

            this.undoMenuItem.Enabled = (this.historyCurrent != this.historyStart);
            this.redoMenuItem.Enabled = (this.historyCurrent != this.historyEnd);

            //Make sure there is something in the clipboard that is valid
            string clipboardText = Clipboard.GetText();
            this.pasteMenuItem.Enabled = (clipboardText != null && validateText(clipboardText));
        }

        /// <summary>
        /// Handles the SelectAll event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_SelectAllClick(Object sender, EventArgs args)
        {
            performSelectAll();
        }

        /// <summary>
        /// Handles the UndoClick event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_UndoClick(Object sender, EventArgs args)
        {
            performUndo();
        }

        /// <summary>
        /// Handles the UndoClick event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_CutClick(Object sender, EventArgs args)
        {
            performCut();
        }

        /// <summary>
        /// Handles the RedoClick event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_RedoClick(Object sender, EventArgs args)
        {
            performRedo();
        }

        /// <summary>
        /// Handles the copy event from the context menu.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_CopyClick(Object sender, EventArgs args)
        {
            performCopy();
        }

        /// <summary>
        /// Handles the RedoClick event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void EVENT_PasteClick(Object sender, EventArgs args)
        {
            performPasta();
        }
        #endregion

        #region Key Events

        /*
         * Apparently if I use the context menu shortcuts I didn't need to do this...but I did...so look at how neat it is...something...I like cake.
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.KeyDown"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyEventArgs"/> that contains the event data.</param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Z)
            {
                //Supress the key pressed even since we are handling it here
                e.SuppressKeyPress = true;

                //Perform the undo action
                performUndo();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Y)
            {
                //Supress the key pressed even since we are handling it here
                e.SuppressKeyPress = true;

                //Perform the redo action
                performRedo();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                //Supress the key pressed even since we are handling it here
                e.SuppressKeyPress = true;

                //Select all the text
                this.SelectAll();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.C)
            {
                //Supress the key pressed even since we are handling it here
                e.SuppressKeyPress = true;

                performCopy();
            }
            else if (e.Modifiers == Keys.Control && e.KeyCode == Keys.V)
            {
                //Supress the key pressed even since we are handling it here
                e.SuppressKeyPress = true;

                performPasta();
            }
        }
        */

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.KeyPress"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.KeyPressEventArgs"/> that contains the event data.</param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            //Get the current number format so we can handle different locales
            NumberFormatInfo nfi = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;

            //Save the key pressed to a string becuase we will use it a bunch later
            string inputValue = e.KeyChar.ToString();

            //Check if the key being sent is a digit or a backspace
            if (Char.IsDigit(e.KeyChar) || e.KeyChar == '\b')
            {
                this.saveNeeded = true;
            }
            //If the key is a negative sign, we allow negatives signs, there isn't already one in the text, and we are at the start write the negative sign
            else if (inputValue == nfi.NegativeSign && this.allowNegatives && !base.Text.Contains(nfi.NegativeSign) && this.SelectionStart == 0)
            {
                this.saveNeeded = true;
            }
            //If the key is a decimal point, we're storing a float, and there isn't already a decimal point
            else if (inputValue == nfi.NumberDecimalSeparator && this.format == NumberFormat.NF_FLOAT && !base.Text.Contains(nfi.NumberDecimalSeparator))
            {
                this.saveNeeded = true;
            }
            else
            {
                //Nomnom the character and beep
                e.Handled = true;
                SystemSounds.Beep.Play();
            }
        }

        #endregion

        #region Actions

        /// <summary>
        /// Performs the select all action.
        /// </summary>
        private void performSelectAll()
        {
            this.SelectAll();
        }

        /// <summary>
        /// Steps back one in the history if there is anything in the history.
        /// If the history is empty a beep noise is played.
        /// </summary>
        private void performUndo()
        {
            if (this.historyCurrent != this.historyStart)
            {
                //Step back one in the history and wrap backwards if we need to
                this.historyCurrent--;
                if (this.historyCurrent < 0)
                {
                    this.historyCurrent = this.history.Length - 1;
                }

                //Set the value to what's in the history
                base.Text = this.history[this.historyCurrent].Value;
                this.SelectionStart = this.history[this.historyCurrent].CursorIndex;

                Console.WriteLine("Start: " + this.historyStart + ", Current: " + this.historyCurrent + ", End: " + this.historyEnd);
            }
            else
            {
                SystemSounds.Beep.Play();
            }
        }

        /// <summary>
        /// Steps forward one in the history if there is anything there
        /// </summary>
        private void performRedo()
        {
            //Make sure there is actually stuff in the history to redo
            if (this.historyCurrent != this.historyEnd)
            {
                this.historyCurrent++;
                if (this.historyCurrent >= this.history.Length)
                {
                    this.historyCurrent = 0;
                }

                //Set the value to what's in the history
                base.Text = this.history[this.historyCurrent].Value;
                this.SelectionStart = this.history[this.historyCurrent].CursorIndex;

                Console.WriteLine("Start: " + this.historyStart + ", Current: " + this.historyCurrent + ", End: " + this.historyEnd);
            }
            else
            {
                SystemSounds.Beep.Play();
            }
        }

        /// <summary>
        /// Performs the copy action.
        /// Checks to see if there is something selected. If so it copies the selected text
        /// </summary>
        private void performCopy()
        {
            if (this.SelectionLength > 0)
            {
                Clipboard.SetText(base.Text.Substring(this.SelectionStart, this.SelectionLength));
            }
        }

        private void performCut()
        {
            if (this.SelectionLength > 0)
            {
                this.saveNeeded = true;
                Clipboard.SetText(base.Text.Substring(this.SelectionStart, this.SelectionLength));
                base.Text = base.Text.Substring(0, this.SelectionStart) + base.Text.Substring(this.SelectionStart + this.SelectionLength);
            }
        }

        /// <summary>
        /// Performs the paste action.
        /// </summary>
        private void performPasta()
        {
            //Get the text from the clipboard
            string clipboardText = Clipboard.GetText();

            if (clipboardText != null)
            {
                //check if it is valid
                if (validateText(clipboardText))
                {
                    string newText;

                    //Check if we should convert it or not
                    if (this.convert)
                    {
                        //Convert the text
                        newText = convertText(clipboardText);
                    }
                    else
                    {
                        newText = clipboardText;
                    }
                    this.saveNeeded = true;

                    int previousStart = this.SelectionStart;
                    base.Text = base.Text.Substring(0, this.SelectionStart) + newText + base.Text.Substring(this.SelectionStart + this.SelectionLength);
                    this.SelectionStart = previousStart + newText.Length;
                }
                else
                {
                    SystemSounds.Beep.Play();
                }
            }
        }
        #endregion

		#endregion Methods 



        #region Menu items

        MenuItem cutMenuItem;
        MenuItem copyMenuItem;
        MenuItem pasteMenuItem;
        MenuItem undoMenuItem;
        MenuItem redoMenuItem;
        MenuItem selectAllMenuItem;
        MenuItem deleteMenuItem;

        #endregion

    }
}
