﻿namespace NumericTextBoxLib
{
    partial class TestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.negativesCheckBox = new System.Windows.Forms.CheckBox();
            this.dragdropCheckBox = new System.Windows.Forms.CheckBox();
            this.conversionCheckBox = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.numericTextBox1 = new NumericTextBoxLib.NumericTextBox();
            this.SuspendLayout();
            // 
            // negativesCheckBox
            // 
            this.negativesCheckBox.AutoSize = true;
            this.negativesCheckBox.Location = new System.Drawing.Point(12, 38);
            this.negativesCheckBox.Name = "negativesCheckBox";
            this.negativesCheckBox.Size = new System.Drawing.Size(102, 17);
            this.negativesCheckBox.TabIndex = 1;
            this.negativesCheckBox.Text = "Allow Negatives";
            this.negativesCheckBox.UseVisualStyleBackColor = true;
            this.negativesCheckBox.CheckedChanged += new System.EventHandler(this.negativesCheckBox_CheckedChanged);
            // 
            // dragdropCheckBox
            // 
            this.dragdropCheckBox.AutoSize = true;
            this.dragdropCheckBox.Location = new System.Drawing.Point(12, 61);
            this.dragdropCheckBox.Name = "dragdropCheckBox";
            this.dragdropCheckBox.Size = new System.Drawing.Size(77, 17);
            this.dragdropCheckBox.TabIndex = 2;
            this.dragdropCheckBox.Text = "Allow Drop";
            this.dragdropCheckBox.UseVisualStyleBackColor = true;
            this.dragdropCheckBox.CheckedChanged += new System.EventHandler(this.dragdropCheckBox_CheckedChanged);
            // 
            // conversionCheckBox
            // 
            this.conversionCheckBox.AutoSize = true;
            this.conversionCheckBox.Location = new System.Drawing.Point(120, 38);
            this.conversionCheckBox.Name = "conversionCheckBox";
            this.conversionCheckBox.Size = new System.Drawing.Size(107, 17);
            this.conversionCheckBox.TabIndex = 3;
            this.conversionCheckBox.Text = "Allow Conversion";
            this.conversionCheckBox.UseVisualStyleBackColor = true;
            this.conversionCheckBox.CheckedChanged += new System.EventHandler(this.conversionCheckBox_CheckedChanged);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Integer",
            "Float"});
            this.comboBox1.Location = new System.Drawing.Point(120, 57);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(107, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // numericTextBox1
            // 
            this.numericTextBox1.AllowNegatives = true;
            this.numericTextBox1.ConvertPastedText = true;
            this.numericTextBox1.Format = NumericTextBoxLib.NumericTextBox.NumberFormat.NF_FLOAT;
            this.numericTextBox1.Location = new System.Drawing.Point(12, 12);
            this.numericTextBox1.Name = "numericTextBox1";
            this.numericTextBox1.Size = new System.Drawing.Size(336, 20);
            this.numericTextBox1.TabIndex = 0;
            // 
            // TestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 120);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.conversionCheckBox);
            this.Controls.Add(this.dragdropCheckBox);
            this.Controls.Add(this.negativesCheckBox);
            this.Controls.Add(this.numericTextBox1);
            this.Name = "TestForm";
            this.Text = "TestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NumericTextBox numericTextBox1;
        private System.Windows.Forms.CheckBox negativesCheckBox;
        private System.Windows.Forms.CheckBox dragdropCheckBox;
        private System.Windows.Forms.CheckBox conversionCheckBox;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}