﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NumericTextBoxLib
{
    public partial class TestForm : Form
    {
        public TestForm()
        {
            InitializeComponent();
            this.negativesCheckBox.Checked = this.numericTextBox1.AllowNegatives;
            this.conversionCheckBox.Checked = this.numericTextBox1.ConvertPastedText;
            this.dragdropCheckBox.Checked = this.numericTextBox1.AllowDrop;
            this.comboBox1.SelectedIndex = (int)this.numericTextBox1.Format;
        }

        private void negativesCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.numericTextBox1.AllowNegatives = this.negativesCheckBox.Checked;
        }

        private void dragdropCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.numericTextBox1.AllowDrop = this.dragdropCheckBox.Checked;
        }

        private void conversionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            this.numericTextBox1.ConvertPastedText = this.conversionCheckBox.Checked;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedIndex == 0)
            {
                this.numericTextBox1.Format = NumericTextBox.NumberFormat.NF_INT;
            }
            else if (this.comboBox1.SelectedIndex == 1)
            {
                this.numericTextBox1.Format = NumericTextBox.NumberFormat.NF_FLOAT;
            }
        }
    }
}
